# Keycloak Server

Utiliza a imagem `jboss/keycloak` com os dados 
```
{
  "realm": "celkrealm",
  "auth-server-url": "http://localhost:8080/auth/",
  "ssl-required": "none",
  "resource": "celkclient",
  "public-client": true,
  "verify-token-audience": true,
  "use-resource-role-mappings": true,
  "confidential-port": 0
}
```
### Usuário
```
{
	username: ricardo.bianchini,
	password: naosei
	e-mail: bianchinirick@gmail.com
}
```