# Projeto Celk Sample

Projeto com login baseado em micro serviços. Utiliza:
- [Servidor keycloak para login](keycloak/README.md)
- [Servidor backend Java Spring Boot com GraphQL & Keycloak client](celksample/README.md)
- [Frontend Vue com keycloak client e axios para requisições](celkweb/README.md)

## Diagramas

 ![usecase](doc/images/usecase.png)

 ![component](doc/images/Component.png)

 ![sequence](doc/images/sequence.png)

 ![deployment](doc/images/deployment.png)

## Exemplo Online
 - URL: http://ec2-18-230-88-57.sa-east-1.compute.amazonaws.com
 - Usuário: ricardo.bianchini
 - Senha: naosei

## API GraphQL

```
type User {
  name: String!
  fullName: String!
  email: String!
}

type Query {
  getLoggedUser : User
}
```

### Testes da API
 ![test_result](doc/images/test_result.png)

 - [Resultado do teste em formato json](doc/result_report.json)
