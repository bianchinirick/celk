import Vue from "vue";
import App from "./App.vue";
import Keycloak from "keycloak-js/dist/keycloak";

Vue.config.productionTip = false;


let initOptions = {
	url: `http://${process.env.VUE_APP_KEYCLOAK_SERVER}:${process.env.VUE_APP_KEYCLOAK_PORT}/auth`, realm: "celkrealm", clientId: "celkclient",  onLoad: "login-required", cors: true
};

let keycloak = Keycloak(initOptions);
Vue.prototype.keycloakCelk = keycloak; 

keycloak.init({ onLoad: initOptions.onLoad }).success((auth) => {

	if (!auth) {
		window.location.reload();
	} 

	localStorage.setItem("vue-token", keycloak.token);
	localStorage.setItem("vue-refresh-token", keycloak.refreshToken);

	new Vue({
		render: h => h(App),
	}).$mount("#app");

	setTimeout(() => {
		keycloak.updateToken(70).then((refreshed) => {
			if (refreshed) {
				Vue.$log.debug("Token refreshed" + refreshed);
			} else {
				Vue.$log.warn("Token not refreshed, valid for "
					+ Math.round(keycloak.tokenParsed.exp + keycloak.timeSkew - new Date().getTime() / 1000) + " seconds");
			}
		}).catch(() => {
			Vue.$log.error("Failed to refresh token");
		});


	}, 60000);

}).error(() => {
	Vue.$log.error("Authenticated Failed");
});










