#!/bin/bash

sed -i s/#__BACKEND_SERVER__#/${BACKEND_SERVER}/g /usr/share/nginx/html/js/app*.js && \
sed -i s/#__BACKEND_PORT__#/${BACKEND_PORT}/g /usr/share/nginx/html/js/app*.js && \
sed -i s/#__KEYCLOAK_SERVER__#/${KEYCLOAK_SERVER}/g /usr/share/nginx/html/js/app*.js && \
sed -i s/#__KEYCLOAK_PORT__#/${KEYCLOAK_PORT}/g /usr/share/nginx/html/js/app*.js

nginx -g 'daemon off;'