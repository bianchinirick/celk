# celkweb

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build

```

### Compiles and create docker image
```
npm run docker-build
```

### Lints and fixes files
```
npm run lint
```
