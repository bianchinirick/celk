package br.com.celk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CelkSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CelkSampleApplication.class, args);
	}

}
