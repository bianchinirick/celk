package br.com.celk.graphql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import br.com.celk.model.User;
import br.com.celk.service.UserService;

@Component
public class Query implements GraphQLQueryResolver {
	
	@Autowired
	private UserService service;

   public User getLoggedUser() {
      return service.getLoggedUser();
   }

}