package br.com.celk.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.Field;
import java.security.Principal;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.adapters.spi.KeycloakAccount;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.celk.graphql.Query;
import br.com.celk.model.User;
import br.com.celk.service.UserService;

@RunWith(SpringRunner.class)
public class SimpleTest {
	
	private static final String EMAIL = "bianchinirick@gmail.com";
	private static final String FULL_NAME = "Ricardo Bianchini";
	private static final String PREFERRED_USERNAME = "ricardo.bianchini";
	
	
	@TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {
		
		private static Logger LOG = LoggerFactory.getLogger(EmployeeServiceImplTestContextConfiguration.class);
		
		@Bean
		public Query query() {
			Query query = new Query();
			try {
				Field declaredFieldService = Query.class.getDeclaredField("service");
				declaredFieldService.setAccessible(true);
				declaredFieldService.set(query, userService());
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				LOG.error(e.getLocalizedMessage(), e);
			}
			return query;
		}
  
		@Bean
        public UserService userService() {
        	KeycloakAccount account = new KeycloakAccount() {
				
				@Override
				public Set<String> getRoles() {
					return Stream.of("ROLE_view-profile").collect(Collectors.toSet());
				}
				
				@Override
				public Principal getPrincipal() {
					RefreshableKeycloakSecurityContext context = new RefreshableKeycloakSecurityContext() {

						private static final long serialVersionUID = 1L;

						@Override
						public AccessToken getToken() {
							AccessToken accessToken = new AccessToken();
							accessToken.setPreferredUsername(PREFERRED_USERNAME);
							accessToken.setName(FULL_NAME);
							accessToken.setEmail(EMAIL);
							return accessToken;
						}
							
					};
					return new KeycloakPrincipal<>(PREFERRED_USERNAME, context);
				}
			};
        	
        	KeycloakAuthenticationToken authentication = new KeycloakAuthenticationToken(account, true);  
        	
        	
        	SecurityContextHolder.getContext().setAuthentication(authentication);
            return new UserService();
        }
    }

	@Autowired 
	private UserService userService;
	
	@Autowired 
	private Query query;
	
	
	@Test
	public void testService() {
		assertThat(userService.getLoggedUser()).isEqualTo(new User(PREFERRED_USERNAME, FULL_NAME, EMAIL));
	}
	
	@Test
	public void testQuery() {
		assertThat(query.getLoggedUser()).isEqualTo(new User(PREFERRED_USERNAME, FULL_NAME, EMAIL));
	}
}
